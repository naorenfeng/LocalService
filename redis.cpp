#include "redis.h"
#include <QDebug>
#include <utils.h>


Redis::Redis()
{

}

/***
 * Redis组件初始化IP和端口，并且连接数据库
 * **/
Redis::Redis(QString ip,int port)
{
    this->m_ip = ip;
    this->m_port = port;
    connectRedis();
}


/***
 * Redis设置IP和端口
 * **/
void Redis::setAddress(QString ip,int port)
{
    this->freeRedis();
    this->m_ip = ip;
    this->m_port = port;
    connectRedis();
}

/***
 * Redis测试，测试本地redis库，连接和读写
 * **/
void Redis::test(){
    // 初始化 ip, 端口
    m_ip = "127.0.0.1";
    m_port = 6379;

    /*
        连接测试
    */

    QString code;

    connectRedis();
    // 设置一个整数
    code = "set miniTest 110";
    m_cmd = code.toLatin1().data();
    execRedis();
    freeReply();

    code = "expire miniTest " + expireTime;
    m_cmd = code.toLatin1().data();
    execRedis();
    freeReply();

    // 获取设置到的整数
    code = "get miniTest";
    m_cmd = code.toLatin1().data();
    execRedis();
    if(m_reply != NULL)
    {
        qDebug().noquote() << QLatin1String(m_reply->str) + "\n"<<endl;
    }
    else
    {
        qDebug().noquote() << "no reply \n"<<endl;
    }

    freeRedis();
}

/***
 * 设置Redis数据，并且超时事件为expireTime
 * **/
void Redis::set(QString key, QString value){
    //设置参数值

    m_cmd.append("set ");
    m_cmd.append(key);
    m_cmd.append(" ");
    m_cmd.append(value);
    execRedis();
    freeReply();

    m_cmd = "";
    m_cmd.append("expire ");
    m_cmd.append(key);
    m_cmd.append(" ");
    m_cmd.append(expireTime);
    execRedis();
    freeReply();
}
/***
 * 获取redis中键为key的字符串数据值
 * **/
QString Redis::get(QString key){

    m_cmd.append("get ");
    m_cmd.append(key);
    m_cmd.append(" ");
    execRedis();
    if(m_reply != NULL)
    {
        QString result(m_reply->str);
        qDebug() << result <<endl;
        return result;
    }
    else
    {
        qDebug() << "no reply \n"<<endl;
        return "";
    }
}


/***
 * 连接 redis
 * **/
void Redis::connectRedis()
{
    m_context = redisConnect(m_ip.toLatin1().data(), m_port);

    if( m_context->err == 0){
        this->connected = true;
        qDebug().noquote() << "redis connect success"<<endl;
    }else{
        this->connected = false;
        qDebug().noquote() << "redis connect error"<<endl;
    }
}

/***
 * 执行 redis 命令
 * **/
void Redis::execRedis()
{
    const char* ch = Utils::Q2C(m_cmd);
    qDebug() << ch;
    m_reply = (redisReply*)redisCommand(m_context, ch );
    m_cmd.clear();
}

/***
 * 释放 redis 资源
 * **/
void Redis::freeRedis()
{
    if(this->connected){
        // 释放数据库连接
        qDebug().noquote() << "redis free"<<endl;
        if(m_context)
        {
           redisFree(m_context);
        }
        qDebug().noquote() << "redis already free"<<endl;
        this->connected = false;
    }
}

/***
 * 释放 reply 指针
 * **/
void Redis::freeReply()
{
    if(m_reply != NULL && this->connected)
    {
        freeReplyObject(m_reply);
    }
}


Redis::~Redis()
{
    freeRedis();
    qDebug().noquote() << "Redis Destoryed"<<endl;
}

