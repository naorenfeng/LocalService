#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMenu>
#include <QAction>
#include <QDebug>

#include <QCheckBox>
#include <QDir>

#include <QMetaObject>
#include <QMetaClassInfo>
#include <QMetaMethod>
#include <typeinfo>

#include <QtDebug>
#include <QFile>
#include <QTextStream>

#include <utils.h>

#include <QPrinter>
#include <QPrintDialog>
#include <QTextDocument>
#include <base64.h>

#include <QProcess>


/***
 * 主题方法，初始化中有以下任务：
 * 创建任务栏图标
 * 创建任务栏小菜单
 * 关联事件和MainWindow方法
 * 设置默认dll存放目录和app基本信息
 * 读取本地配置：
 *      redis的ip和地址 默认 127.0.0.1  6379
 *      udp监听端口 默认 9368
 *      程序是否开机启动 默认 false
 *      返回信息的间隔符 默认 |->
 * 初始化UDP监听，开启监听
 * 创建provider，初始化provider用于调用读取dll/打印/二维码等服务
 * 设置开机启动和开机启动勾选关联方法
 *
 * 程序生成完毕
 * ***/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // 创建任务栏图标
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":/logo.ico"));
    trayIcon->setVisible(true);
    connect(trayIcon, &QSystemTrayIcon::activated, this, &MainWindow::onTrayIconActivated);
    trayIcon->setContextMenu(new QMenu(this));
    //任务栏小菜单功能
    trayIcon->contextMenu()->addAction(QIcon(":/pics/测试.png"),"测试", this, &MainWindow::b_test_clicked);
    trayIcon->contextMenu()->addAction(QIcon(":/pics/显示.png"),"显示主界面",this,&MainWindow::show);
    trayIcon->contextMenu()->addAction(QIcon(":/pics/退出.png"),"退出程序", this, &MainWindow::closeApp);

    // 组件事件与方法关联区   b_UDPSend
    connect(this->findChild<QPushButton *>("b_RedisReadWrite"),&QPushButton::clicked,this,&MainWindow::b_RedisReadWrite_clicked);
    connect(this->findChild<QPushButton *>("b_UDPBegin"),&QPushButton::clicked,this,&MainWindow::b_UDPBegin_clicked);
    connect(this->findChild<QPushButton *>("b_UDPEnd"),&QPushButton::clicked,this,&MainWindow::b_UDPEnd_clicked);
    connect(this->findChild<QPushButton *>("b_test"),&QPushButton::clicked,this,&MainWindow::b_test_clicked);
    connect(this->findChild<QAction *>("a_mini"),&QAction::triggered,this,&MainWindow::a_mini_triggered);
    connect(this->findChild<QAction *>("a_close"),&QAction::triggered,this,&MainWindow::a_close_triggered);
    connect(this->findChild<QCheckBox *>("c_openWithSystem"),&QCheckBox::toggled,this,&MainWindow::c_openWithSystem_toggled);
    connect(this->findChild<QLineEdit *>("t_psw"),&QLineEdit::textEdited,this,&MainWindow::t_psw_entered);

    //设置dll存放目录，默认在exe目录下的 /dll 文件夹内
    dir = qApp->applicationDirPath();
    appName = "LocalService";
    appSource = dir + "/" + appName + ".exe";

    //读取本地配置，文件默认在exe目录下 如果不存在就设置默认值
    settings = new QSettings("LocalService.ini",QSettings::IniFormat);
    QFileInfo fileInfo(dir + "/LocalService.ini");
    if(fileInfo.isFile()){
        qDebug()<<dir + "/LocalService.ini  读取中！"<<endl;

        //获取Redis数据库的IP和Port
        QString ip = settings->value("redis/ip").toString();
        int port   = settings->value("redis/port").toInt();
        if(ip.isEmpty() || port <= 0){
            qDebug() << dir + "/LocalService.ini 中未配置Redis数据库地址！" << endl;
            redis = new Redis("127.0.0.1",6379);
            settings->setValue("redis/ip","127.0.0.1");
            settings->setValue("redis/port",6379);
        }else{
            redis = new Redis(ip,port);
        }

        // 获取UDP监听端口配置
        udpPort = settings->value("udp/port").toString();
        if(udpPort.isEmpty()){
            udpPort = "9368";
            settings->setValue("udp/port","9368");
        }
        ui->t_newUDPPort->setText(udpPort);

        // 获取是否随系统启动配置
        QString sWS = settings->value("system/open").toString();
        if(sWS.isEmpty()){
            openWithSystem = false;
            settings->setValue("system/open","false");
        }else{
            if(sWS.trimmed() == "true"){
                openWithSystem = true;
            }else{
                openWithSystem = false;
            }
        }
        ui->c_openWithSystem->setChecked(this->openWithSystem);

        // 获取接收数据和返回数据的间隔符 默认为 |->
        this->split = settings->value("system/split").toString();
        if(this->split.isEmpty()){
            this->split = "|->";
            settings->setValue("system/split","|->");
        }

        // 获取更新服务器信息，默认是FTP方式更新
        this->updateServiceURL = settings->value("update/url").toString();
        this->updateType = settings->value("update/type").toString().toLower();// ftp http
        if(this->updateType.isEmpty()) this->updateType = "ftp";


    }else{
        qDebug()<<dir + "/LocalService.ini  文件不存在！"<<endl;
        redis = new Redis("127.0.0.1",6379);
        udpPort = "9368";
        openWithSystem = false;
        this->split = "|->";

        settings->setValue("redis/ip","127.0.0.1");
        settings->setValue("redis/port",6379);
        settings->setValue("udp/port","9368");
        settings->setValue("system/open","false");
        settings->setValue("system/split","|->");
    }
    settings->sync();

    //配置了更新地址则自动更新
    if(!this->updateServiceURL.isEmpty()){
        this->version = this->app->arguments().first();
        if(this->version.isEmpty()){
            this->autoUpdate();
        }
    }

    qApp->setQuitOnLastWindowClosed(false);

    //UDP相关
    udpSendSocket = new QUdpSocket(this);
    udpReceiveSocket = new QUdpSocket(this);
    connect(udpReceiveSocket,SIGNAL(readyRead()),
                this,SLOT(onSocketReadyRead()));
    this->udpListening = false;
    this->udpBegin();
    //ServiceProvider 设置
    provider = new ServiceProvider(dir,this->split);

    //设置组件状态
    this->findChild<QCheckBox *>("c_openWithSystem")->setChecked(this->openWithSystem);

    //设置开机启动
    this->setOpenWithSystem();

    //设置窗口性质
    this->setWindowFlags(windowFlags()&~Qt::WindowMaximizeButtonHint);    // 禁止最大化按钮
    this->setFixedSize(570,410);                     // 禁止拖动窗口大小


    qDebug()<<"程序主窗口生成"<<endl;

}

/***
 * 窗口销毁：
 *  udp连接丢弃
 *  删除相关组件
 * ***/
MainWindow::~MainWindow()
{
    udpReceiveSocket->abort();
    udpSendSocket->abort();

    delete redis;
    delete provider;
    delete udpSendSocket;
    delete udpReceiveSocket;
    delete ui;
}

/***
 * 测试方法
 *
* ***/
void MainWindow::b_test_clicked(){

    qDebug()<< "<< 测试";
    qDebug("()测试");
    QPrinter printer;
    printer.setPageSize(QPrinter::A4);
//    QPrintDialog printDialog(&printer, this);
//    if (printDialog.exec()){
        QTextDocument textDocument;
        QString html("<!DOCTYPE html> <html lang=\"zh-cn\"> <head> <meta charset=\"utf-8\" /> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /> <meta name=\"referrer\" content=\"origin-when-cross-origin\" /> <meta name=\"description\" content=\"1.使用html生成表格QString wesiOperateWidget::MakeDataToHtml(){ //表头 //html = &amp;quot;&amp;quot;; //html &#x2B;= &amp;quot;&amp;quot;; //html &#x2B;= &amp;quot;编号&amp;quot;; //html &#x2B;= &amp;quot\" /> <meta property=\"og:description\" content=\"1.使用html生成表格QString wesiOperateWidget::MakeDataToHtml(){ //表头 //html = &amp;quot;&amp;quot;; //html &#x2B;= &amp;quot;&amp;quot;; //html &#x2B;= &amp;quot;编号&amp;quot;; //html &#x2B;= &amp;quot\" /> <meta http-equiv=\"Cache-Control\" content=\"no-transform\" /> <meta http-equiv=\"Cache-Control\" content=\"no-siteapp\" /> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /> <title>使用QPrinter生成pdf或用打印机打印数据 - 恒月美剑 - 博客园</title> <link id=\"favicon\" rel=\"shortcut icon\" href=\"//common.cnblogs.com/favicon.svg\" type=\"image/svg+xml\" /> <link rel=\"stylesheet\" href=\"/css/blog-common.min.css?v=KD3VL7wgYZC8816mU3zvd6ffO4njzgotBUq7XnWAtJ0\" /> <link id=\"MainCss\" rel=\"stylesheet\" href=\"/skins/blacklowkey/bundle-blacklowkey.min.css?v=1gj9-XJKL9BZFkjdvROYQPiPWjQXk-hsXXZGtqkfaSc\" /> <link id=\"highlighter-theme-cnblogs\" type=\"text/css\" rel=\"stylesheet\" href=\"/css/hljs/cnblogs.css?v=5J1NDtbnnIr2Rc2SdhEMlMxD4l9Eydj88B31E7_NhS4\" /> <link id=\"mobile-style\" media=\"only screen and (max-width: 767px)\" type=\"text/css\" rel=\"stylesheet\" href=\"/skins/blacklowkey/bundle-blacklowkey-mobile.min.css?v=lsinHqYJW5ImNMpCjALOBBC9q7lOQZVhJMDRHS9Avxs\" /> <link type=\"application/rss+xml\" rel=\"alternate\" href=\"https://www.cnblogs.com/jck34/rss\" /> <link type=\"application/rsd+xml\" rel=\"EditURI\" href=\"https://www.cnblogs.com/jck34/rsd.xml\" /> <link type=\"application/wlwmanifest+xml\" rel=\"wlwmanifest\" href=\"https://www.cnblogs.com/jck34/wlwmanifest.xml\" /> <script> var currentBlogId = 129889; var currentBlogApp = 'jck34'; var isLogined = false; var isBlogOwner = false; var skinName = 'BlackLowKey'; var visitorUserId = ''; var hasCustomScript = false; try { if (hasCustomScript && document.referrer && document.referrer.indexOf('baidu.com') >= 0) { Object.defineProperty(document, 'referrer', { value: '' }); Object.defineProperty(Document.prototype, 'referrer', { get: function () { return ''; } }); } } catch (error) { } window.cb_enable_mathjax = false; window.mathEngine = 0; window.codeHighlightEngine = 1; window.enableCodeLineNumber = false; window.codeHighlightTheme = 'cnblogs'; window.darkModeCodeHighlightTheme = 'vs2015'; window.isDarkCodeHighlightTheme = false; window.isDarkModeCodeHighlightThemeDark = true; window.isDisableCodeHighlighter = false; window.enableCodeThemeTypeFollowSystem = false; window.enableMacStyleCodeBlock = false </script> <script> var currentPostDateAdded = '2014-11-19 11:47'; </script> <script src=\"https://common.cnblogs.com/scripts/jquery-2.2.0.min.js\"></script> <script src=\"https://www-cdn.cnblogs.com/js/blog-common.min.js?v=qlNqYbAQsAnenT0jJlBjFxJuJ25DJzcELoLR4q3Q6ug\"></script> </head> <body class=\"skin-blacklowkey has-navbar has-bannerbar\"> <a name=\"top\"></a> <a href=\"https://www.cnblogs.com/cmt/p/17489281.html\" onclick=\"countCreativeClicks('C0-行行-直播第2期')\" target=\"_blank\" rel=\"nofollow\"> <div class=\"bannerbar forpc\" style=\"background-size: contain;background-image: url(https://img2023.cnblogs.com/blog/35695/202306/35695-20230618172511947-867540764.jpg);margin-left: -5px;\"> <img src=\"https://img2023.cnblogs.com/blog/35695/202306/35695-20230618103826805-1030742738.jpg\" onload=\"countCreativeImpressions('C0-行行-直播第2期')\" /> <span id=\"c0_impression\" style=\"display:none\"></span> </div> </a> <div id=\"bannerbar\" class=\"bannerbar-mobile bannerbar-text-mobile formobile\"> <a href=\"https://www.cnblogs.com/cmt/p/17489281.html\" onclick=\"countCreativeClicks('M2-行行-直播第2期')\" rel=\"nofollow\"> <img src=\"https://img2023.cnblogs.com/blog/35695/202306/35695-20230618173101599-940677134.jpg\" alt=\"\" onload=\"countCreativeImpressionsOnMobile('M2-行行-直播第2期')\" /> <span id=\"m2_impression\" style=\"display:none\"></span> </a> </div> <div id=\"top_nav\" class=\"navbar forpc\"> <nav id=\"nav_main\" class=\"navbar-main\"> <ul id=\"nav_left\" class=\"navbar-list navbar-left\"> <li class=\"navbar-branding\"> <a href=\"https://www.cnblogs.com/\" title=\"开发者的网上家园\" role=\"banner\"> <img src=\"//common.cnblogs.com/logo.svg\" alt=\"博客园Logo\" /> </a> </li> <li> <a href=\"/\" onclick=\"countClicks('skin-navbar-sitehome')\">首页</a> </li> <li> <a href=\"https://news.cnblogs.com/\" onclick=\"countClicks('nav', 'skin-navbar-news')\">新闻</a> </li> <li> <a href=\"https://q.cnblogs.com/\" onclick=\"countClicks('nav', 'skin-navbar-q')\">博问</a> </li> <li><a href=\"https://www.cnblogs.com/cmt/p/17489281.html\">直播</a></li> <li> <a href=\"https://ing.cnblogs.com/\" onclick=\"countClicks('nav', 'skin-navbar-ing')\">闪存</a> </li> <li> <a href=\"https://edu.cnblogs.com/\" onclick=\"countClicks('nav', 'skin-navbar-edu')\">班级</a> </li> </ul> <ul id=\"nav_right\" class=\"navbar-list navbar-right\"> <li> <form id=\"zzk_search\" class=\"navbar-search dropdown\" action=\"https://zzk.cnblogs.com/s\" method=\"get\" role=\"search\"> <input name=\"w\" id=\"zzk_search_input\" placeholder=\"代码改变世界\" type=\"search\" tabindex=\"3\" autocomplete=\"off\" /> <button id=\"zzk_search_button\" onclick=\"window.navbarSearchManager.triggerActiveOption()\"> <img id=\"search_icon\" class=\"focus-hidden\" src=\"//common.cnblogs.com/images/blog/search.svg\" alt=\"搜索\" /> <img class=\"hidden focus-visible\" src=\"/images/enter.svg\" alt=\"搜索\" /> </button> <ul id=\"navbar_search_options\" class=\"dropdown-menu quick-search-menu\"> <li tabindex=\"0\" class=\"active\" onclick=\"zzkSearch(event, document.getElementById('zzk_search_input').value)\"> <div class=\"keyword-wrapper\"> <img src=\"//common.cnblogs.com/images/blog/search.svg\" alt=\"搜索\" /> <div class=\"keyword\"></div> </div> <span class=\"search-area\">所有博客</span> </li> <li tabindex=\"1\" onclick=\"zzkBlogSearch(event, 'jck34', document.getElementById('zzk_search_input').value)\"> <div class=\"keyword-wrapper\"> <img src=\"//common.cnblogs.com/images/blog/search.svg\" alt=\"搜索\" /> <div class=\"keyword\"></div> </div> <span class=\"search-area\">当前博客</span> </li> </ul> </form> </li> <li id=\"navbar_login_status\" class=\"navbar-list\"> <a class=\"navbar-user-info navbar-blog\" href=\"https://i.cnblogs.com/EditPosts.aspx?opt=1\" alt=\"写随笔\" title=\"写随笔\"> <img id=\"new_post_icon\" class=\"navbar-icon\" src=\"//common.cnblogs.com/images/blog/newpost.svg\" alt=\"写随笔\" /> </a> <a id=\"navblog-myblog-icon\" class=\"navbar-user-info navbar-blog\" href=\"https://passport.cnblogs.com/GetBlogApplyStatus.aspx\" alt=\"我的博客\" title=\"我的博客\"> <img id=\"myblog_icon\" class=\"navbar-icon\" src=\"//common.cnblogs.com/images/blog/myblog.svg\" alt=\"我的博客\" /> </a> <a class=\"navbar-user-info navbar-message navbar-icon-wrapper\" href=\"https://msg.cnblogs.com/\" alt=\"短消息\" title=\"短消息\"> <img id=\"msg_icon\" class=\"navbar-icon\" src=\"//common.cnblogs.com/images/blog/message.svg\" alt=\"短消息\" /> <span id=\"msg_count\" style=\"display: none\"></span> </a> <a id=\"navbar_lite_mode_indicator\" data-current-page=\"blog\" style=\"display: none\" href=\"javascript:void(0)\" alt=\"简洁模式\" title=\"简洁模式启用，您在访问他人博客时会使用简洁款皮肤展示\"> <img class=\"navbar-icon\" src=\"//common.cnblogs.com/images/blog/lite-mode-on.svg\" alt=\"简洁模式\" /> </a> <div id=\"user_info\" class=\"navbar-user-info dropdown\"> <a class=\"dropdown-button\" href=\"https://home.cnblogs.com/\"> <img id=\"user_icon\" class=\"navbar-avatar\" src=\"//common.cnblogs.com/images/blog/avatar-default.svg\" alt=\"用户头像\" /> </a> <div class=\"dropdown-menu\"> <a id=\"navblog-myblog-text\" href=\"https://passport.cnblogs.com/GetBlogApplyStatus.aspx\">我的博客</a> <a href=\"https://home.cnblogs.com/\">我的园子</a> <a href=\"https://account.cnblogs.com/settings/account\">账号设置</a> <a href=\"javascript:void(0)\" id=\"navbar_lite_mode_toggle\" title=\"简洁模式会使用简洁款皮肤显示所有博客\"> 简洁模式 <img id=\"navbar_lite_mode_on\" src=\"/images/lite-mode-check.svg\" class=\"hide\" /><span id=\"navbar_lite_mode_spinner\" class=\"hide\">...</span> </a> <a href=\"javascript:void(0)\" onclick=\"account.logout();\">退出登录</a> </div> </div> <a class=\"navbar-anonymous\" href=\"https://account.cnblogs.com/signup\">注册</a> <a class=\"navbar-anonymous\" href=\"javascript:void(0);\" onclick=\"account.login()\">登录</a> </li> </ul> </nav> </div> <!--done--> <div id=\"home\"> <div id=\"header\"> <div id=\"blogTitle\"> <a id=\"lnkBlogLogo\" href=\"https://www.cnblogs.com/jck34/\"><img id=\"blogLogo\" src=\"/skins/custom/images/logo.gif\" alt=\"返回主页\" /></a> <!--done--> <h1><a id=\"Header1_HeaderTitle\" class=\"headermaintitle HeaderMainTitle\" href=\"https://www.cnblogs.com/jck34/\">恒月美剑</a> </h1> <h2></h2> </div><!--end: blogTitle 博客的标题和副标题 --> <div id=\"navigator\"> <ul id=\"navList\"> <li><a id=\"blog_nav_sitehome\" class=\"menu\" href=\"https://www.cnblogs.com/\"> 博客园</a> </li> <li> <a id=\"blog_nav_myhome\" class=\"menu\" href=\"https://www.cnblogs.com/jck34/\"> 首页</a> </li> <li> <a id=\"blog_nav_newpost\" class=\"menu\" href=\"https://i.cnblogs.com/EditPosts.aspx?opt=1\"> 新随笔</a> </li> <li> </li> <li> <a id=\"blog_nav_rss\" class=\"menu\" href=\"javascript:void(0)\" data-rss=\"https://www.cnblogs.com/jck34/rss/\"> 订阅</a> <!--<partial name=\"./Shared/_XmlLink.cshtml\" model=\"Model\" /></li>--></li> <li> <a id=\"blog_nav_admin\" class=\"menu\" href=\"https://i.cnblogs.com/\"> 管理</a> </li> </ul> <div class=\"blogStats\"> <div id=\"blog_stats_place_holder\"><script>loadBlogStats();</script></div> </div><!--end: blogStats --> </div><!--end: navigator 博客导航栏 --> </div><!--end: header 头部 --> <div id=\"main\"> <div id=\"mainContent\"> <div class=\"forFlow\"> <div id=\"post_detail\"> <!--done--> <div id=\"topics\"> <div class=\"post\"> <h1 class = \"postTitle\"> <a id=\"cb_post_title_url\" class=\"postTitle2 vertical-middle\" href=\"https://www.cnblogs.com/jck34/p/4107885.html\"> <span role=\"heading\" aria-level=\"2\">使用QPrinter生成pdf或用打印机打印数据</span> </a> </h1> <div class=\"clear\"></div> <div class=\"postBody\"> <div id=\"cnblogs_post_body\" class=\"blogpost-body blogpost-body-html\"> <p>1.使用html生成表格</p> <div class=\"cnblogs_code\"><img id=\"code_img_closed_2f0c748e-9f76-49ec-9fe0-ec96683e480b\" class=\"code_img_closed\" src=\"https://images.cnblogs.com/OutliningIndicators/ContractedBlock.gif\" alt=\"\"><img id=\"code_img_opened_2f0c748e-9f76-49ec-9fe0-ec96683e480b\" class=\"code_img_opened\" style=\"display: none\" src=\"https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif\" alt=\"\"> <div id=\"cnblogs_code_open_2f0c748e-9f76-49ec-9fe0-ec96683e480b\" class=\"cnblogs_code_hide\"> <pre><span style=\"color: rgba(0, 0, 0, 1)\">QString wesiOperateWidget::MakeDataToHtml() { </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">表头 </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html = \"&lt;table width=100% border=1 cellspacing=0 text-align=center style=border-collapse:collapse&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;tr&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;th&gt;编号&lt;/th&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;th&gt;姓名&lt;/th&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;th&gt;头部成绩&lt;/th&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;th&gt;胸部成绩&lt;/th&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;/tr&gt;\";</span> <span style=\"color: rgba(128, 128, 128, 1)\">///</span><span style=\"color: rgba(0, 128, 0, 1)\">/数据</span> <span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;tr&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;td align=center&gt;1&lt;/td&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;td align=center&gt;赵六&lt;/td&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;td align=center&gt;100&lt;/td&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;td align=center&gt;400&lt;/td&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;/tr&gt;\"; </span><span style=\"color: rgba(0, 128, 0, 1)\">//</span><span style=\"color: rgba(0, 128, 0, 1)\">html += \"&lt;/table&gt;\";</span> <span style=\"color: rgba(0, 0, 0, 1)\">    QString html; html </span>= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;table width=100% border=1 cellspacing=0 text-align=center style=border-collapse:collapse&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;tr&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;th&gt;编号&lt;/th&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;th&gt;姓名&lt;/th&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;th&gt;头部成绩&lt;/th&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;th&gt;胸部成绩&lt;/th&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;/tr&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; QStringList dataStringList; QStringList rowInfoList; html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;tr&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; </span><span style=\"color: rgba(0, 0, 255, 1)\">foreach</span><span style=\"color: rgba(0, 0, 0, 1)\">(QString str, dataStringList) { rowInfoList </span>= str.split(<span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">,</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">); </span><span style=\"color: rgba(0, 0, 255, 1)\">for</span>(<span style=\"color: rgba(0, 0, 255, 1)\">int</span> i = <span style=\"color: rgba(128, 0, 128, 1)\">0</span>; i &lt; <span style=\"color: rgba(128, 0, 128, 1)\">8</span>; i += <span style=\"color: rgba(128, 0, 128, 1)\">2</span><span style=\"color: rgba(0, 0, 0, 1)\">) { </span><span style=\"color: rgba(0, 0, 255, 1)\">if</span>(rowInfoList[i] == <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">number</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">) { html </span>= html + <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;td align=center&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span> + rowInfoList[i + i] + <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;/td&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; } </span><span style=\"color: rgba(0, 0, 255, 1)\">else</span> <span style=\"color: rgba(0, 0, 255, 1)\">if</span>(rowInfoList[i] == <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">name</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">) { html </span>= html + <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;td align=center&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span> + rowInfoList[i + i] + <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;/td&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; } </span><span style=\"color: rgba(0, 0, 255, 1)\">else</span> <span style=\"color: rgba(0, 0, 255, 1)\">if</span>(rowInfoList[i] == <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">headscore</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">) { html </span>= html + <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;td align=center&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span> + rowInfoList[i + i] + <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;/td&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; } </span><span style=\"color: rgba(0, 0, 255, 1)\">else</span> <span style=\"color: rgba(0, 0, 255, 1)\">if</span>(rowInfoList[i] == <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">bodyscore</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">) { html </span>= html + <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;td align=center&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span> + rowInfoList[i + i] + <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;/td&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; } } } html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;/tr&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; html </span>+= <span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">&lt;/table&gt;</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">; </span><span style=\"color: rgba(0, 0, 255, 1)\">return</span><span style=\"color: rgba(0, 0, 0, 1)\"> html; }</span></pre> </div> <span class=\"cnblogs_code_collapse\">view code</span></div> <p>2.生成pdf文档</p> <div class=\"cnblogs_code\"><img id=\"code_img_closed_d133538e-1dc1-4297-9dbd-0bdcd62ed732\" class=\"code_img_closed\" src=\"https://images.cnblogs.com/OutliningIndicators/ContractedBlock.gif\" alt=\"\"><img id=\"code_img_opened_d133538e-1dc1-4297-9dbd-0bdcd62ed732\" class=\"code_img_opened\" style=\"display: none\" src=\"https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif\" alt=\"\"> <div id=\"cnblogs_code_open_d133538e-1dc1-4297-9dbd-0bdcd62ed732\" class=\"cnblogs_code_hide\"> <pre><span style=\"color: rgba(0, 0, 0, 1)\">    QPrinter printer; printer.setPageSize(QPrinter::A4); printer.setOutputFormat(QPrinter::PdfFormat); QString filepath </span>= QFileDialog::getSaveFileName(<span style=\"color: rgba(0, 0, 255, 1)\">this</span>, tr(<span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">保存为...</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span>), tr(<span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">D://成绩.pdf</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span>), tr(<span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">PDF格式(*.pdf)</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(0, 0, 0, 1)\">)); printer.setOutputFileName(filepath); QTextDocument textDocument; textDocument.setHtml(MakeDataToHtml()); textDocument.print(</span>&amp;<span style=\"color: rgba(0, 0, 0, 1)\">printer); QMessageBox::about(</span><span style=\"color: rgba(0, 0, 255, 1)\">this</span>, tr(<span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">提示</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span>), tr(<span style=\"color: rgba(128, 0, 0, 1)\">\"</span><span style=\"color: rgba(128, 0, 0, 1)\">保存成功</span><span style=\"color: rgba(128, 0, 0, 1)\">\"</span>));</pre> </div> <span class=\"cnblogs_code_collapse\">View Code</span></div> <p>3.使用打印机打印</p> <div class=\"cnblogs_code\"><img id=\"code_img_closed_ce7beaa2-b702-41b6-bb1c-0db71f11f44c\" class=\"code_img_closed\" src=\"https://images.cnblogs.com/OutliningIndicators/ContractedBlock.gif\" alt=\"\"><img id=\"code_img_opened_ce7beaa2-b702-41b6-bb1c-0db71f11f44c\" class=\"code_img_opened\" style=\"display: none\" src=\"https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif\" alt=\"\"> <div id=\"cnblogs_code_open_ce7beaa2-b702-41b6-bb1c-0db71f11f44c\" class=\"cnblogs_code_hide\"> <pre><span style=\"color: rgba(0, 128, 128, 1)\">1</span> <span style=\"color: rgba(0, 0, 0, 1)\">    QPrinter printer; </span><span style=\"color: rgba(0, 128, 128, 1)\">2</span> <span style=\"color: rgba(0, 0, 0, 1)\">    printer.setPageSize(QPrinter::A4); </span><span style=\"color: rgba(0, 128, 128, 1)\">3</span>     QPrintDialog printDialog(&amp;printer, <span style=\"color: rgba(0, 0, 255, 1)\">this</span><span style=\"color: rgba(0, 0, 0, 1)\">); </span><span style=\"color: rgba(0, 128, 128, 1)\">4</span>     <span style=\"color: rgba(0, 0, 255, 1)\">if</span><span style=\"color: rgba(0, 0, 0, 1)\"> (printDialog.exec()){ </span><span style=\"color: rgba(0, 128, 128, 1)\">5</span> <span style=\"color: rgba(0, 0, 0, 1)\">        QTextDocument textDocument; </span><span style=\"color: rgba(0, 128, 128, 1)\">6</span> <span style=\"color: rgba(0, 0, 0, 1)\">        textDocument.setHtml(MakeDataToHtml()); </span><span style=\"color: rgba(0, 128, 128, 1)\">7</span>         textDocument.print(&amp;<span style=\"color: rgba(0, 0, 0, 1)\">printer); </span><span style=\"color: rgba(0, 128, 128, 1)\">8</span>     }</pre> </div> <span class=\"cnblogs_code_collapse\">View Code</span></div> <p>&nbsp;</p> </div> <div class=\"clear\"></div> <div id=\"blog_post_info_block\" role=\"contentinfo\"> <div id=\"blog_post_info\"></div> <div class=\"clear\"></div> <div id=\"post_next_prev\"></div> </div> </div> <div class=\"postDesc\">posted @ <span id=\"post-date\" data-last-update-days=\"3135.907360063955\" data-date-created=\"BlogServer.Application.Dto.BlogPost.BlogPostDto\" data-date-updated=\"2014-11-19 11:47\">2014-11-19 11:47</span>&nbsp; <a href=\"https://www.cnblogs.com/jck34/\">恒月美剑</a>&nbsp; 阅读(<span id=\"post_view_count\">7626</span>)&nbsp; 评论(<span id=\"post_comment_count\">0</span>)&nbsp; <a href=\"https://i.cnblogs.com/EditPosts.aspx?postid=4107885\" rel=\"nofollow\">编辑</a>&nbsp; <a href=\"javascript:void(0)\" onclick=\"AddToWz(4107885);return false;\">收藏</a>&nbsp; <a href=\"javascript:void(0)\" onclick=\"reportManager.report({ currentUserId: '', targetType: 'blogPost', targetId: '4107885', targetLink: 'https://www.cnblogs.com/jck34/p/4107885.html', title: '使用QPrinter生成pdf或用打印机打印数据' })\">举报</a> </div> </div> </div><!--end: topics 文章、评论容器--> </div> <script> var cb_entryId = 4107885, cb_entryCreatedDate = '2014-11-19 11:47', cb_postType = 1, cb_postTitle = '使用QPrinter生成pdf或用打印机打印数据'; var allowComments = true, cb_blogId = 129889, cb_blogApp = 'jck34', cb_blogUserGuid = 'fa1e24e0-c7fc-e011-b7b9-842b2b196315'; mermaidRender.render() markdown_highlight() zoomManager.apply(\"#cnblogs_post_body img:not(.code_img_closed):not(.code_img_opened)\"); updatePostStats( [cb_entryId], function(id, count) { $(\"#post_view_count\").text(count) }, function(id, count) { $(\"#post_comment_count\").text(count) }); </script> <a id=\"!comments\"></a> <div id=\"blog-comments-placeholder\"></div> <div id=\"comment_form\" class=\"commentform\"> <a name=\"commentform\"></a> <div id=\"divCommentShow\"></div> <div id=\"comment_nav\"><span id=\"span_refresh_tips\"></span><a href=\"javascript:void(0);\" onclick=\"return RefreshCommentList();\" id=\"lnk_RefreshComments\" runat=\"server\" clientidmode=\"Static\">刷新评论</a><a href=\"#\" onclick=\"return RefreshPage();\">刷新页面</a><a href=\"#top\">返回顶部</a></div> <div id=\"comment_form_container\"></div> <div class=\"ad_text_commentbox\" id=\"ad_text_under_commentbox\"></div> <div id=\"cnblogs_ch\"></div> <div id=\"opt_under_post\"></div> <div id=\"cnblogs_c1\" class=\"under-post-card\"> <div id='div-gpt-ad-1592365906576-0' style='width: 300px; height: 250px;'></div> </div> <div id=\"under_post_card1\"></div> <div id=\"under_post_card2\"></div> <div id=\"HistoryToday\" class=\"under-post-card\"></div> <script type=\"text/javascript\"> var commentManager = new blogCommentManager(); commentManager.renderComments(0); fixPostBody(); window.footnoteTipManager.generateFootnoteTips(); window.tocManager.displayDisableTocTips = false; window.tocManager.generateToc(); setTimeout(function() { countViews(cb_blogId, cb_entryId); }, 50); deliverT2(); deliverC1C2(); loadNewsAndKb(); LoadPostCategoriesTags(cb_blogId, cb_entryId); LoadPostInfoBlock(cb_blogId, cb_entryId, cb_blogApp, cb_blogUserGuid); GetPrevNextPost(cb_entryId, cb_blogId, cb_entryCreatedDate, cb_postType); loadOptUnderPost(); GetHistoryToday(cb_blogId, cb_blogApp, cb_entryCreatedDate); </script> </div> </div><!--end: forFlow --> </div><!--end: mainContent 主体内容容器--> <div id=\"sideBar\"> <div id=\"sideBarMain\"> <div id=\"sidebar_c3\"></div> <div id=\"blog-calendar\" style=\"display:none\"></div><script>loadBlogDefaultCalendar();</script> <div id=\"leftcontentcontainer\"> <div id=\"blog-sidecolumn\"></div> <script>loadBlogSideColumn();</script> </div> </div><!--end: sideBarMain --> </div><!--end: sideBar 侧边栏容器 --> <div class=\"clear\"></div> </div><!--end: main --> <div class=\"clear\"></div> <div id=\"footer\"> <!--done--> Copyright &copy; 2023 恒月美剑 <br /><span id=\"poweredby\">Powered by .NET 7.0 on Kubernetes</span> </div><!--end: footer --> </div><!--end: home 自定义的最大容器 --> <input type=\"hidden\" id=\"antiforgery_token\" value=\"CfDJ8DuWIYDefEVJtUW7VadsHY-2Fye1QezlhEEVzwf4BIAto_lRZOfX9wGbBy4fu2ScJJQPOtCOBAXoucEDxhmBLc81RuqDLBYJl6L4KXZX5UpyTUIgn9ryKlS7Jppum0cU5xIXgBETXNMFddHH72Kdpjo\" /> <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-476124-1\"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); var kv = getGACustom(); if (kv) { gtag('set', kv); } gtag('config', 'UA-476124-1'); </script> <script defer src=\"https://hm.baidu.com/hm.js?866c9be12d4a814454792b1fd0fed295\"></script> </body> </html>");
        textDocument.setHtml(html);
        textDocument.print(&printer);
//    }
}


/***
 * redis读写测试
 *
* ***/
void MainWindow::b_RedisReadWrite_clicked(){
    qDebug()<<"Redis读写测试按钮被按下"<<endl;
    //获取输入框地址，进行redis数据库连接，并且进行数据库key value的写入后读取测试
    if(redis->connected){
        QString saveKey = ui->t_SaveKey->text();
        QString saveVal = ui->t_SaveValue->text();
        if(saveKey.isEmpty() || saveVal.isEmpty()){
            QMessageBox::information(this,"Redis读写测试","数据Key和Value不能为空！");
        }else{
            Base64::Encrypt_s(saveVal);
            redis->set(saveKey,saveVal);
            QString result = redis->get(saveKey);
            if(result.isEmpty()){result = "";}
            Base64::Decrypt_s(result);
            ui->t_Result->setText(result);
            QMessageBox::information(this,"Redis读写测试","读写完成！");
        }
    }else{
        QMessageBox::information(this,"Redis读写测试","Redis数据库尚未连接！");
    }
}


/***
 * UDP监听开始
 *
* ***/
bool MainWindow::udpBegin(){
    if(!udpListening){
        quint16 port = this->udpPort.toInt();
        if(udpReceiveSocket->bind(port)){
            this->udpListening = true;
            return true;
        }else{
            this->udpListening = false;
            return false;
        }
    }
    return true;
}

/***
 * udp监听结束
 *
* ***/
bool MainWindow::udpEnd(){
    if(udpListening){
        udpReceiveSocket->close();
        this->udpListening = false;
    }
    return true;
}

/***
 * udp监听开始按钮事件
 *
* ***/
void MainWindow::b_UDPBegin_clicked(){
    if(!udpListening){
        if(udpBegin()){
            QMessageBox::information(this,"UDP监听测试","开始监听端口[" + this->udpPort + "]！");
        }else{
            QMessageBox::information(this,"UDP监听测试","绑定端口[" + this->udpPort + "]失败！");
        }
    }

}

/***
 * udp监听结束按钮事件
 *
* ***/
void MainWindow::b_UDPEnd_clicked(){
    if(udpListening){
        udpEnd();
        QMessageBox::information(this,"UDP监听测试","UDP监听结束！");

    }
}

/***
 * 开机启动事件
 * **/
void MainWindow::c_openWithSystem_toggled(){
    if(ui->c_openWithSystem->checkState() == this->openWithSystem) return;
    if(ui->c_openWithSystem->checkState()){
        // 开机启动
        this->openWithSystem = true;
        settings->setValue("system/open","true");
    }else{
        this->openWithSystem = false;
        settings->setValue("system/open","false");
    }
    this->setOpenWithSystem();
    settings->sync();
    QMessageBox::information(this,"开机启动设置","设置成功！");
}
/***
 * 设置开机启动的函数
 * ***/
#define AUTO_RUN "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Run"
void MainWindow::setProcessAutoRun(const QString &appPath,bool flag)
{
    QSettings settings(AUTO_RUN, QSettings::NativeFormat);

    //以程序名称作为注册表中的键,根据键获取对应的值（程序路径）
    QFileInfo fInfo(appPath);
    QString name = fInfo.baseName();    //键-名称

    //如果注册表中的路径和当前程序路径不一样，则表示没有设置自启动或本自启动程序已经更换了路径
    QString oldPath = settings.value(name).toString(); //获取目前的值-绝对路劲
    QString newPath = QDir::toNativeSeparators(appPath);    //toNativeSeparators函数将"/"替换为"\"
    if(flag)
    {
        if (oldPath != newPath)
            settings.setValue(name, newPath);
    }
    else
       settings.remove(name);
}

/***
 * 设置开机启动状态
 * ***/
void MainWindow::setOpenWithSystem(){
    if(openWithSystem){
        setProcessAutoRun(this->appSource,1);
    }else{
        setProcessAutoRun(this->appSource,0);
    }
}

/***
 * UDP监听事件
 * 如果是测试，接收信息为 "DEBUG"
 * 正常接收
 * redis 数据的ID |-> 服务名称 |-> 调用方法名 |-> 参数列表...
 * 正常返回
 * 方法返回值 |-> 参数列表...
 * 返回中参数列表包含了回调函数返回信息
 * 处理UDP信息会是单线程，当处理UDP信息时，dealing将会设置为true停止处理后续的UDP信息
 * ***/
void MainWindow::onSocketReadyRead(){
    while(udpReceiveSocket->hasPendingDatagrams() && !dealing)
       {
           QByteArray datagram;
           datagram.resize(udpReceiveSocket->pendingDatagramSize());  //数据格式统一

           QHostAddress peerAddr;
           quint16 peerPort;
           udpReceiveSocket->readDatagram(datagram.data(),
                                   datagram.size(),&peerAddr,&peerPort);   //接收数据
           QString str(datagram.data());    //数据转换为QT的ui界面使用的QString类型
           qDebug() << str;
           Base64::Decrypt_s(str);
           qDebug() << str;
           if(!str.isEmpty() || str.indexOf(this->split) != -1){
               //当执行相关命令时，就废弃掉处理期间的所有命令  命令进行base64解密
               command = str;
               qDebug() << command;
               //命令格式： 服务名
               QStringList cmdList = str.split(this->split);
               if(cmdList[0] == "DEBUG"){
                   ui->t_UDPInfo->setText(ui->t_UDPInfo->toPlainText() + this->containCommands(cmdList,1) + " \n" );
               }else{
                   this->service(cmdList);
               }
           }
       }
}

/***
 *
 * 对指令进行操作
 * 调用对应服务，获取返回结果
 * 写入redis中
 *
 * **/
void MainWindow::service(QStringList cmd)
{
    // 入参： redis中的数据ID|->服务名称|->函数名|->服务参数1|->服务参数2 ...
    // 出餐： 方法返回参数|->传入参数1|->传入参数2|->传入参数3 ...  返回传入参数是为了解决
    this->dealing = true;

    QString id = cmd[0];
    if(id.isEmpty()){
        return;
    }
    cmd.removeFirst();
    QString result = provider->service(cmd,this->split);
    Base64::Encrypt_s(result);
    redis->set(id,result);
    qDebug()<< "id:"+id+" \n result:"+result <<endl;
    this->dealing = false;
    return;
}

/***
 * 封装的方法，用split指定的符号隔开返回结果，组装成一条装进redis的字符串数据
 *
 * ***/
QString MainWindow::containCommands(QStringList list,int begin){
    QString str;
    if(list.size() < begin){return "";}
    for(int i = 0; i< list.size();++i)
    {
        if(i >= begin){
            if(str.isEmpty()){
                str = list[i];
            }else{
                str.append(this->split);
                str.append(list[i]);
            }
        }
    }
    return str;
}
/***
 * 窗口最小化
 * ***/
void MainWindow::a_mini_triggered(){
    this->winhide();
}

/***
 * 窗口关闭
 * ***/
void MainWindow::a_close_triggered(){
    this->closeApp();
}

/***
 * 双击任务栏小图标，显示和隐藏窗口
 * ***/
void MainWindow::onTrayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::DoubleClick) {
        // 双击任务栏图标显示/隐藏主页面
        if (isVisible()) {
            this->winhide();
        } else {
            this->winshow();
        }
    }
}

/***
 * 页面解锁，固定密码 lingyl
 * ***/
void MainWindow::t_psw_entered(QString key){
    QString value = this->ui->t_psw->text();
    if(key == "lingyl"){
        this->ui->frame->hide();
    }
}

/***
 * 窗口隐藏，并且上锁
 * ***/
void MainWindow::winhide(){
    hide();
    this->ui->t_psw->setText("");
    this->ui->frame->showNormal();
}

/***
 * 窗口显示，并且上锁
 * ***/
void MainWindow::winshow(){
    showNormal();
    this->ui->t_psw->setText("");
    this->ui->frame->showNormal();
}

/***
 * 关闭程序，回收相关资源
 * ***/
void MainWindow::closeApp()
{
    // 回收相关资源
    qDebug().noquote() << "Redis 连接 释放开始"<<endl;
    redis->freeRedis();
    qDebug().noquote() << "Redis 连接 释放结束"<<endl;
    this->udpEnd();
    qDebug().noquote() << "udp结束监听"<<endl;

    qDebug()<<"closing"<<endl;
    app->exit(0);
}

/***
 * 窗口关闭事件，此事件不会退出程序，而是隐藏页面
 * ***/
void MainWindow::closeEvent(QCloseEvent *event)
{
    // 关闭事件交由任务栏图标处理
    event->ignore();
    this->winhide();
}

/***
 * 服务 调用测试 按钮事件
 *
* ***/

void MainWindow::on_b_servictest_clicked()
{
    QStringList params;
    QString servicename = this->ui->t_servicename->text();
    QString funname = this->ui->t_funname->text();
    if(servicename.isEmpty() || funname.isEmpty()){
        this->ui->t_servicereturn->setText("服务名和调用的函数不能为空！");
        return;
    }
    params.append(servicename);
    params.append(funname);
    params.append(this->ui->t_param1->text());
    params.append(this->ui->t_param2->text());
    params.append(this->ui->t_param3->text());
    params.append(this->ui->t_param4->text());
    params.append(this->ui->t_param5->text());
    params.append(this->ui->t_param6->text());
    params.append(this->ui->t_param7->text());
    params.append(this->ui->t_param8->text());
    QString res = provider->service(params,this->split);
    if(res.isEmpty()){
        this->ui->t_servicereturn->setText("返回结果为空！");
    }else{
        this->ui->t_servicereturn->setText(res);
    }
    return ;
}

/***
 * udp信息发送测试按钮事件
 *
* ***/
void MainWindow::on_b_UDPSend_clicked()
{
    qDebug()<<"发送UDP测试信息";
    quint16 targetPort = this->udpPort.toInt();
    QString str = "DEBUG" + this->split + this->ui->t_UDPSend->toPlainText();
    if(str.isEmpty()){
        str =  QString("DEBUG" + this->split + "测试信息").toUtf8();
    }
    Base64::Encrypt_s(str);
    qDebug()<<str;
    QHostAddress targetAddr(QString("127.0.0.1"));
    udpSendSocket->writeDatagram(str.toUtf8(), targetAddr, targetPort);
}


/***
 * redis连接测试
 *
* ***/
void MainWindow::on_b_TestRedis_clicked()
{
    qDebug()<<"Redis连接测试按钮被按下"<<endl;
    QString address = ui->t_RedisAddress->text();
    if(!address.isEmpty()){
        Redis * red;
        red = new Redis(address,6379);
        if(red->connected){
            red->freeRedis();
            QMessageBox::information(this,"Redis连接测试","连接成功！");
        }else{
            QMessageBox::information(this,"Redis连接测试","连接失败！");
        }
    }
}

/***
 * 将测试成功的Redis设置到当前
 *
 * **/
void MainWindow::on_b_SetRedis_clicked()
{
    qDebug()<<"Redis设置按钮被按下"<<endl;
    QString address = ui->t_RedisAddress->text();
    int port = ui->t_RedisPort->text().toInt();
    if(!address.isEmpty() && port > 0){
        Redis * red;
        red = new Redis(address,port);
        if(red->connected){
            red->freeRedis();
            redis = new Redis(address,port);
            settings->setValue("redis/ip",address);
            settings->setValue("redis/port",port);
            settings->sync();
            QMessageBox::information(this,"Redis设置","设置成功！");
        }else{
            QMessageBox::information(this,"Redis设置","连接失败，无法设置！！");
        }
    }else{
        QMessageBox::information(this,"Redis设置","请输入正确的ip和端口！");
    }
}

/***
 * 更换当前监听的端口，并且保存到本地文件
 * **/
void MainWindow::on_b_updateUDPPort_clicked()
{
    if(!this->udpEnd()){
        QMessageBox::information(this,"UDP监听端口设置","关闭当前UDP监听失败！");
        return;
    }
    QString port = ui->t_newUDPPort->text();
    if(port.isEmpty()){
        QMessageBox::information(this,"UDP监听端口设置","端口不能为空！");
        return;
    }
    this->udpPort = port;
    if(!this->udpBegin()){
        QMessageBox::information(this,"UDP监听端口设置","启动UDP监听失败！");
        return;
    }
    settings->setValue("udp/port",port);
    settings->sync();

    QMessageBox::information(this,"UDP监听端口设置","设置成功！");

}

/***
 * 确定可以给更新，然后打开自动更新程序，并且关闭当前程序
 * ***/
void MainWindow::autoUpdate()
{
    QString program = dir + "/" + "autoUpdate.exe";
    QFileInfo updateFile(program);
    if(updateFile.exists() && !this->updateServiceURL.isEmpty()){
        QStringList arguments;
        QProcess updateProcess;
        arguments<< this->updateServiceURL << this->updateType << this->appSource ;
        updateProcess.start(program, arguments);
        this->closeApp();
    }
}
