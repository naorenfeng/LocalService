#------------------------------------------------- 
# Project created by QtCreator 2023-04-07T17:41:30 
#------------------------------------------------- 
QT       += core gui network printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
 
TARGET = LocalService
TEMPLATE = app

RC_FILE = logo.rc
RCC_DIR = tmp/rcc

SOURCES += main.cpp \
    mainwindow.cpp \
    redis.cpp \
    utils.cpp \
    qrencode/bitstream.c \
    qrencode/mask.c \
    qrencode/mmask.c \
    qrencode/mqrspec.c \
    qrencode/qrencode.c \
    qrencode/qrinput.c \
    qrencode/qrspec.c \
    qrencode/rsecc.c \
    qrencode/split.c \
    qrcodedialog.cpp \
    printer.cpp \
    base64.cpp \
    serviceprovider.cpp

HEADERS += mainwindow.h \
    redis.h \
    utils.h \
    includes/alloc.h \
    includes/async.h \
    includes/async_private.h \
    includes/dict.h \
    includes/fmacros.h \
    includes/hiredis.h \
    includes/hiredis_ssl.h \
    includes/net.h \
    includes/read.h \
    includes/sds.h \
    includes/sdsalloc.h \
    includes/sockcompat.h \
    includes/win32.h \
    qrencode/bitstream.h \
    qrencode/config.h \
    qrencode/mask.h \
    qrencode/mmask.h \
    qrencode/mqrspec.h \
    qrencode/qrencode.h \
    qrencode/qrencode_inner.h \
    qrencode/qrinput.h \
    qrencode/qrspec.h \
    qrencode/rsecc.h \
    qrencode/split.h \
    qrcodedialog.h \
    printer.h \
    base64.h \
    serviceprovider.h
	
FORMS    += mainwindow.ui \
    qrcodedialog.ui

INCLUDEPATH += ./includes

DISTFILES += \
    logo.rc \
    lib/libQtHiRedis.a \
    lib/QtHiRedis.dll \
    dll/test.dll \
    dll/testDll.dll \
    logo.rc


RESOURCES += \
    res.qrc

DEFINES += HAVE_CONFIG_H


#-------------------------------------------------
# Redis 
#-------------------------------------------------

unix|win32: LIBS += -L$$PWD/lib/ -lQtHiRedis

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/QtHiRedis.lib
else:unix|win32-g++: PRE_TARGETDEPS += $$PWD/lib/libQtHiRedis.a

