#include "mainwindow.h"
#include <QApplication>
#include <QTime>
#include <QFileInfo>
#include <QDir>


void customMessageHandler(QtMsgType type,const QMessageLogContext & context, const QString &msg)
{
    QString txt;
    switch (type) {
    //调试信息提示
    case QtDebugMsg:
        txt = QString("Debug: %1").arg(msg);
        break;

    //一般的warning提示
    case QtWarningMsg:
        txt = QString("Warning: %1").arg(msg);
        break;
    //严重错误提示
    case QtCriticalMsg:
        txt = QString("Critical: %1").arg(msg);
        break;
    //致命错误提示
    case QtFatalMsg:
        txt = QString("Fatal: %1").arg(msg);
        abort();
    }
    QDate date = QDate::currentDate();
    QTime time = QTime::currentTime();
    QString today = date.toString("yyyyMMdd");
    QString logDir = qApp->applicationDirPath() + "/logs";
    QString logFileDir = logDir + "/log_" + today + ".log";
    QDir dir;
    QFile outFile(logFileDir);
    if(!dir.exists(logDir))
    {
        dir.mkdir(logDir);
    }
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    txt = date.toString("yyyy-MM-dd") + " " + time.toString("hh:mm:ss:sss") + "  " + txt;
    ts << txt << endl;
    outFile.close();
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow w;
    w.hide();
    w.app = &app;

    //注册日志输出
    qInstallMessageHandler(customMessageHandler);
    //解决子窗口关闭导致线程退出的问题
    app.setQuitOnLastWindowClosed(false);

    return app.exec();
}
