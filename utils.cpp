#include "utils.h"

#include <QTime>

#include <QDebug>

Utils::Utils()
{

}
Utils::~Utils()
{

}

QString Utils::int2Str(int i)
{
    return QString::number(i);
}

QString Utils::long2Str(long l)
{
    return QString::number(l);
}

QString Utils::float2Str(float f)
{

    return QString::number(f);
}

QString Utils::double2Str(double b)
{

    return QString::number(b);
}

QString Utils::char2Str(char* c)
{
    QString str(c);
    return str;
}

QString Utils::bool2Str(bool b)
{
    if(b){
        return "true";
    }else{
        return "false";
    }
}

/***
 * 将传入的动态数据全部变成字符串并且用间隔符split串联组成字符串返回
 * **/
QString Utils::dataContain(QVariantList list,QString split)
{
    QString result("");
    for (auto i = list.begin(); i != list.end(); ++i)
    {
        QVariant value = *i;
        result.append(value.toString());
        if(i != list.end() - 1)
        {
            result.append(split);
        }
    }
    return result;
}

/***
 * 将传入的动态数据全部变成字符串并且用间隔符,逗号串联组成字符串返回
 * **/
QString Utils::dataContain(QVariantList list)
{
    QString split(",");
    return Utils::dataContain(list,split);
}
/***
 * 获取当前日期
 * **/
QString Utils::today(){
    QTime time = QTime::currentTime();
    return time.toString("YYYYMMDD");
}

/***
 * 从char转换数据，解决中文乱码
 * **/
QString Utils::C2Q(char * str){
    QString result = QString::fromLocal8Bit(str);
    return result.toUtf8();
}

/***
 * 从QString转换数据，解决中文乱码
 * **/
QString Utils::Q2Q(QString str){
    QByteArray qba = str.toLatin1().data();
    QString result = QString::fromLocal8Bit(qba);
    return result;
}

/***
 * 从QString转换数据，解决中文乱码
 * **/
char * Utils::Q2C(QString str){
    std::string s = str.toStdString();
    const char* ch = s.c_str();
    unsigned int size = strlen(ch);
    char *  result = new char[size + 1];
    memcpy(result,ch,size);
    result[size] = 0;
    return result;
}

/***
 * 从QVariant 转 QString 再转换数据，解决中文乱码
 * **/
char * Utils::Q2C(QVariant str){
    std::string s = str.toString().toStdString();
    const char* ch = s.c_str();
    unsigned int size = strlen(ch);
    char *  result = new char[size + 1];
    memcpy(result,ch,size);
    result[size] = 0;
    return result;
}
