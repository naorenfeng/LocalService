#include "qrcodedialog.h"
#include "ui_QRcodeDialog.h"

#include <QPainter>

QRcodeDialog::QRcodeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QRcodeDialog)
{
    this->state = 2;
    this->timer = 60;

    t = new QTimer();

    this->t->setTimerType(Qt::TimerType::PreciseTimer);
    connect(this->t,SIGNAL(timeout()),this,SLOT(timerFun()));

    ui->setupUi(this);

    this->setWindowFlags(Qt::Dialog|Qt::FramelessWindowHint);

    this->hideWin();
}

void QRcodeDialog::hideWin()
{
    QPixmap pixmap(":/pics/qrcode.png");
    this->ui->l_qrcode->setPixmap(pixmap);
    this->ui->l_info->setText("提示信息");
    this->ui->t_qrcode->setText("");
    this->hide();
}

void QRcodeDialog::showWin()
{
    this->result = "";
    this->show();
    this->t->start(1000);
}

void QRcodeDialog::timerFun()
{
    if(this->timer >= 0){
        if(this->timer == 0){
            this->ui->l_time->setText(Utils::int2Str(this->timer));
            this->t->stop();
            this->hideWin();
        }else{
            this->timer--;
            this->ui->l_time->setText(Utils::int2Str(this->timer));
            if(this->state == 1){
                this->ui->t_qrcode->setFocus();
            }
        }
    }
}

QRcodeDialog::~QRcodeDialog()
{

}


void QRcodeDialog::closeWin()
{
    if(this->result.isEmpty()){
        this->done(QDialog::Accepted);
    }else{
        this->done(QDialog::Rejected);
    }
}

void QRcodeDialog::stateToggle()
{
    if(this->state == 2){
        this->readQRcode();
    }else{
        this->showQRcode("");
    }
}


void QRcodeDialog::readQRcode()
{
    this->state = 1;
    this->ui->l_info->setText("请在设备上展示二维码!");
    this->ui->l_qrcode->setPixmap(QPixmap(":/pics/Loading.png"));
    this->ui->t_qrcode->setText("");
    this->ui->t_qrcode->setFocus();
    this->ui->l_time->setText(Utils::int2Str(this->timer));

    this->showWin();
}

void QRcodeDialog::showQRcode(QString code)
{
    if(code.isEmpty()){
        this->result = "1";
        this->closeWin();
    }
    this->state = 2;
    this->ui->l_info->setText("请扫描二维码!");
    this->ui->t_qrcode->hide();
    this->ui->l_time->setText(Utils::int2Str(this->timer));

    this->GenerateQRcode(code,this->ui->l_qrcode);

    this->showWin();
}

void QRcodeDialog::setTimer(int t)
{
    if(t<=0){
        this->timer = -1;
    }else{
        this->timer = t;
    }
}

/*trmpst:二维码包含的信息
 * label：显示二维码的QLabel控件
 * */
void QRcodeDialog::GenerateQRcode(QString tempstr, QLabel *label)
{
    QRcode *qrcode; //二维码数据
    //将QString转化为const char * |2-QR码版本为2 | QR_ECLEVEL_Q 容错等级 |QR_MODE_8 八字节数据 |1-区分大小写
    qrcode = QRcode_encodeString(tempstr.toStdString().c_str(), 2, QR_ECLEVEL_Q, QR_MODE_8, 1);
    qint32 temp_width=label->width(); //显示二维码所用的QLabel大小，也是后面显示二维码图片的大小
    qint32 temp_height=label->height();
    qint32 qrcode_width = qrcode->width > 0 ? qrcode->width : 1;    //生成的二维码宽高（正方形的宽度=高度）
    double scale_x = (double)temp_width / (double)qrcode_width; //二维码图片的缩放比例
    double scale_y =(double) temp_height /(double) qrcode_width;
    QImage mainimg=QImage(temp_width,temp_height,QImage::Format_ARGB32);
    QPainter painter(&mainimg);
    QColor background(Qt::white);
    painter.setBrush(background);
    painter.setPen(Qt::NoPen);
    painter.drawRect(0, 0, temp_width, temp_height);
    QColor foreground(Qt::black);
    painter.setBrush(foreground);
    for( qint32 y = 0; y < qrcode_width; y ++)//qrcode->data是一个存了qrcode_width*qrcode_width个数据的一维数组
    {                                         //这里是把这个一维数组以每行qrcode_width个数据，以二维数组的形式表现出来
        for(qint32 x = 0; x < qrcode_width; x++)
        {
            unsigned char b = qrcode->data[y * qrcode_width + x];
            if(b & 0x01)
            {//根据二维码中黑白点（1/0），在QLabel上以缩放比例画出二维码
                QRectF r(x * scale_x, y * scale_y, scale_x, scale_y);
                painter.drawRects(&r, 1);
            }
        }
    }
    QPixmap mainmap=QPixmap::fromImage(mainimg);
    label->setPixmap(mainmap);
    label->setVisible(true);
}

/*trmpst:二维码包含的信息
 * label：显示二维码的QLabel控件
 * logo：二维码中间显示的图片
 * scale：中间图片的缩放比例
 * */
void QRcodeDialog::GenerateQRcode(QString tempstr, QLabel *label, const QString &logo, float scale)
{
    GenerateQRcode(tempstr,  label);
    int width = label->width();
    int height = label->height();
    int logo_width = width *scale;
    int logo_height =  height * scale;
    int logo_x = (width - logo_width) / 2;
    int logo_y = (width - logo_height) / 2;

    const QPixmap *pix = label->pixmap();
    QPixmap temppix(logo);
    QPixmap pix1 = temppix.scaled(QSize(logo_width, logo_height), Qt::KeepAspectRatio);
    QPixmap pix2(width, height);

    QPainter *painter = new QPainter(&pix2);
    QColor background(Qt::white);
    painter->setBrush(background);
    painter->setPen(Qt::NoPen);
    painter->drawRect(0, 0, width, height);
    QColor foreground(Qt::black);
    painter->setBrush(foreground);

    painter->drawPixmap(0,0, width, height, *pix);
    painter->drawPixmap(logo_x,logo_y, logo_width, logo_height, pix1);
    label->setPixmap(pix2);
    delete painter;
}


void QRcodeDialog::on_b_closeqrcode_clicked()
{
    this->closeWin();
}

void QRcodeDialog::on_t_qrcode_editingFinished()
{
    //防止触发两次
    if(ui->t_qrcode->hasFocus()){}
    //当焦点还存在的时候，不做处理，即不响应回车，只响应鼠标焦点，防止响应两次
    else
    {
        this->result = this->ui->t_qrcode->text();
        this->closeWin();
    }
}
