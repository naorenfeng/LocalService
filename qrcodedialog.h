#ifndef QRCODEDIALOG_H
#define QRCODEDIALOG_H

#include <QDialog>
#include <QTimer>
#include <utils.h>
#include <QDebug>
#include <QMovie>
#include <QLabel>
#include <qrencode/qrencode.h>

namespace Ui {
class QRcodeDialog;
}

class QRcodeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QRcodeDialog(QWidget *parent = 0);
    ~QRcodeDialog();

    void hideWin();
    void showWin();
    void closeWin();
    void setTimer(int t);

    void stateToggle();
    void readQRcode();
    void showQRcode(QString url);

    //二维码创建
    void GenerateQRcode(QString tempstr, QLabel *label);
    void GenerateQRcode(QString tempstr, QLabel *label, const QString &logo, float scale);


    int timer;

    QString result;
public slots:
    void timerFun();

private slots:
    void on_b_closeqrcode_clicked();

    void on_t_qrcode_editingFinished();

private:
    Ui::QRcodeDialog *ui;
    int state;
    QTimer * t;


};

#endif // QRCODEDIALOG_H
