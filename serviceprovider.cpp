#include "serviceprovider.h"


#include <QDebug>
#include <QFileInfo>
#include <QMetaMethod>
#include <QMetaObject>
#include <QList>
#include <utils.h>
#include <QTextCodec>
#include <qrcodedialog.h>
#include <QPrinter>
#include <QTextDocument>

ServiceProvider::ServiceProvider()
{
    this->split = "|->";
}
ServiceProvider::~ServiceProvider()
{

}

ServiceProvider::ServiceProvider(QString Dir,QString split)
{
    this->dir = Dir.append("/dll/");
    this->split = split;
}

/***
 * 初始化时要设置 dir 程序路径
 * split默认为  |-> 可以动态设置
 ***/
ServiceProvider::ServiceProvider(QString Dir)
{
    this->dir = Dir.append("/dll/");
    this->split = "|->";
}

void ServiceProvider::setDir(QString EXEDir){
    this->dir = EXEDir.append("/dll/");
}

void ServiceProvider::setSplit(QString s)
{
    this->split = s;
}


/***
 * 返回错误信息
 ***/
QString ServiceProvider::errInfo(QString info)
{
    return "ERROR" + this->split + info;
}


/***
 * 统一入口
 * 第一个和第二个参数组成需要调用的方法名： dll名称_调用方法名
 * 调用方法统一格式：
 *  QVariant ServiceProvider::dll名_调用方法名(QVariantList p)
 ***/
QString ServiceProvider::service(QStringList args,QString split)
{
    if(args.size()<1){
        return errInfo("统一入口信息为空");
    }
    if(!split.isEmpty()){this->split = split;}
    QString methodName  = args[0] + "_" + args[1];
    QMetaMethod method;
    int count = this->metaObject()->methodCount();
    int methodIndex = -1;
    for(int i = 0;i < count;i++)
    {
        method = this->metaObject()->method(i);
        if(methodName.toUtf8().constData() == method.name())
        {
             methodIndex = i;
             break;
        }
    }
    if(methodIndex == -1)
    {
        return errInfo("无法找到对应DLL方法定义");
    }
    // 根据方法参数类型将入参转换成类型
    QVariantList parameters;
    for (int i = 2; i < args.size(); ++i) {
        QVariant parameter(args[i]);
        parameters.append(parameter);
    }
    QVariant result;
    method.invoke(this, Qt::DirectConnection, Q_RETURN_ARG(QVariant,result),
                   Q_ARG(QVariantList,parameters));
    return result.toString();
}
/****
 * 判断dll文件是否存在
 ***/
bool ServiceProvider::dllExists(QString dllName)
{
    QFileInfo fileInfo(this->dir + dllName);
    if(fileInfo.exists())
    {
        if(fileInfo.isFile())
        {
            return true;
        }
    }
    return false;
}

/***
 * 测试 testDll.dll 的 multiply 方法
 ***/
typedef int (*multiply)(int a,int b);
QVariant ServiceProvider::testDll_multiply(QVariantList p)
{
    if(p.size()<2){
        return errInfo("参数个数不足");
    }
    QString dll("testDll.dll");
    if(!dllExists(dll))
    {
        return errInfo("找不到DLL(" + this->dir + dll + ")");
    }
    int a = p[0].toInt();
    int b = p[1].toInt();
    QLibrary *lib = new QLibrary(this->dir + dll);
    if(lib->load()){
        multiply fun = (multiply)lib->resolve("multiply");
        int result = fun(a,b);
        QVariantList resultList;
        resultList << result << a << b;
        return Utils::dataContain(resultList,this->split);
    }else{
        return errInfo("调用DLL失败！");
    }
}

/***
 * 测试 test.dll 的 ADDD 方法
 ***/
typedef int (*ADDD)(int aaa,int bbb);
QVariant ServiceProvider::test_ADDD(QVariantList p)
{
    if(p.size()<2){
        return errInfo("参数个数不足");
    }
    QString dll("test.dll");
    if(!dllExists(dll))
    {
        return errInfo("找不到DLL(" + this->dir + dll + ")");
    }
    int a = p[0].toInt();
    int b = p[1].toInt();
    QLibrary *lib = new QLibrary(this->dir + dll);
    if(lib->load()){
        ADDD fun = (ADDD)lib->resolve("ADDD");
        int result = fun(a,b);
        QVariantList resultList;
        resultList << result << a << b;
        return Utils::dataContain(resultList,this->split);
    }else{
        return errInfo("调用DLL失败！");
    }
}

/***
 * 测试 test.dll 的 instead 方法
 ***/
typedef char* (*instead)(char* aaa,char* &bbb);
QVariant ServiceProvider::test_instead(QVariantList p)
{
    if(p.size()<2){
        return errInfo("参数个数不足");
    }
    QString dll("test.dll");
    if(!dllExists(dll))
    {
        return errInfo("找不到DLL(" + this->dir + dll + ")");
    }
    char * aa = Utils::Q2C(p[0]);
    char * bb = Utils::Q2C(p[1]);
    QLibrary *lib = new QLibrary(this->dir + dll);
    if(lib->load()){
        instead fun = (instead)lib->resolve("instead");
        char * result = fun(aa,bb);
        QVariantList resultList;
        resultList << result << aa << bb;
        return Utils::dataContain(resultList,this->split);
    }else{
        return errInfo("调用DLL失败！");
    }
}

QVariant ServiceProvider::test_test(QVariantList p)
{
    qDebug() << p;
    return "test";
}

/***
 * 根据传入的字符串生成临时二维码
 * 打开窗口展示二维码
 ***/
QVariant ServiceProvider::qrcode_show(QVariantList p){

    if(p.size() < 1){
        return errInfo("参数个数不足");
    }
    QString str = p[0].toString();
    if(str.isEmpty()){
        return errInfo("二维码字符串不能为空！");
    }
    QRcodeDialog * qrcode = new QRcodeDialog();
    qrcode->showQRcode(str);
    int ret = qrcode->exec();
    QVariantList resultList;
    resultList << ret;
    return  Utils::dataContain(resultList,this->split);
}

/***
 * 打开窗口读取二维码结果字符串
 ***/
QVariant ServiceProvider::qrcode_read(QVariantList p){
    QRcodeDialog * qrcode = new QRcodeDialog();
    qrcode->readQRcode();
    int ret = qrcode->exec();
    QString result;
    if(ret == QDialog::Rejected){
        return errInfo("取消读取操作");
    }else{
        result = qrcode->result;
    }
    QVariantList resultList;
    resultList << result ;
    return Utils::dataContain(resultList,this->split);
}
/****
 * 直接调用默认打印机打印 html 字符串内容
 * **/
QVariant ServiceProvider::print_direct(QVariantList p)
{
    if(p.size() < 1){
        return errInfo("参数个数不足");
    }
    QString html = p[0].toString();
    QPrinter printer;
    printer.setPageSize(QPrinter::A4);
    QTextDocument textDocument;
    textDocument.setHtml(html);
    textDocument.print(&printer);

    QVariantList resultList;
    resultList << true ;
    return Utils::dataContain(resultList,this->split);
}

