#ifndef LIBREADER_H
#define LIBREADER_H

#include <QString>
#include <QStringList>
#include <QLibrary>
#include <QVariant>

class ServiceProvider : public QObject
{
    Q_OBJECT
public:
    ServiceProvider();
    ServiceProvider(QString Dir,QString split);
    ServiceProvider(QString Dir);
    ~ServiceProvider();
    void setDir(QString EXEDir);
    void setSplit(QString s);
    bool dllExists(QString dllName);
    QString service(QStringList params,QString split);

    QString errInfo(QString info);
    QString codeC(char * str);
    QString codeS(QString str);

    char * variant2char(QVariant source);

    // DLL 具体方法调用
    //测试方法群
    Q_INVOKABLE QVariant testDll_multiply(QVariantList p);
    Q_INVOKABLE QVariant test_ADDD(QVariantList p);
    Q_INVOKABLE QVariant test_instead(QVariantList p);
    Q_INVOKABLE QVariant test_test(QVariantList p);
    //二维码窗口，展示和读取二维码
    Q_INVOKABLE QVariant qrcode_show(QVariantList p);
    Q_INVOKABLE QVariant qrcode_read(QVariantList p);
    //打印服务
    Q_INVOKABLE QVariant print_direct(QVariantList p);




private:
    QString dir ;
    QString split;




};

#endif // LIBREADER_H
