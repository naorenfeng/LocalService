#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QPushButton>
#include <QMessageBox>
#include <QString>
#include <QSettings>
#include <QUdpSocket>
#include <QHash>
#include <qrcodedialog.h>

#include "serviceprovider.h"
#include "redis.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QApplication *app;
    bool isTestingRedis=false;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event) override;
    void closeApp();
    void b_TestRedis_clicked();
    void b_RedisReadWrite_clicked();
    void b_test_clicked();
    void b_UDPBegin_clicked();
    void b_UDPEnd_clicked();
    void b_servicetest_clicked();
    void a_mini_triggered();
    void a_close_triggered();
    void t_psw_entered(QString key);
    void c_openWithSystem_toggled();
    bool isUpdatable();
    void autoUpdate();

    void service(QStringList cmd);


    bool udpBegin();
    bool udpEnd();

    QString containCommands(QStringList list,int begin);
    void setProcessAutoRun(const QString &appPath,bool flag);
    void setOpenWithSystem();
    void winhide();
    void winshow();



private slots:
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason reason); // 任务栏图标双击事件
    void onSocketReadyRead();//读取socket传入的数据


    void on_b_servictest_clicked();

    void on_b_UDPSend_clicked();

    void on_b_TestRedis_clicked();

    void on_b_SetRedis_clicked();

    void on_b_updateUDPPort_clicked();

private:
    Ui::MainWindow * ui;
    //右下角
    QSystemTrayIcon * trayIcon;
    //服务封装信息
    ServiceProvider * provider;
    //redis服务类
    Redis * redis;
    //程序当前目录 和程序名
    QString dir;
    QString appName;
    QString appSource;
    //更新服务器地址,程序更新确认类型(ftp,http),当前版本
    QString updateServiceURL;
    QString updateType;
    QString version;

    QSettings * settings;

    QUdpSocket * udpSendSocket;
    QUdpSocket * udpReceiveSocket;
    //udp是否在监听
    bool udpListening;

    //udp监听的端口
    QString udpPort;

    //是否开机启动
    bool openWithSystem;
    //是否在处理数据当中
    bool dealing;
    //当前正在执行的命令字符串 和指令分割符
    QString command;
    QString split;


};

#endif // MAINWINDOW_H
