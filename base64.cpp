#include "base64.h"


 //加密字符串
QString Base64::Encrypt(QString strInfo)
{
    QByteArray ba = strInfo.toUtf8().toBase64();
    QString result(ba);
    return result;
}

void Base64::Encrypt_s(QString &strInfo)
{
    QByteArray ba = strInfo.toUtf8().toBase64();
    strInfo = ba;
}

//解密字符串
QString Base64::Decrypt(QString strInfo)
{
    QByteArray ba = QByteArray::fromBase64(strInfo.toUtf8());
    QString result(ba);
    return result;
}

void Base64::Decrypt_s(QString &strInfo)
{
    QByteArray ba = QByteArray::fromBase64(strInfo.toUtf8());
    strInfo = ba;
}
