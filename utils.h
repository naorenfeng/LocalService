#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QString>
#include <QVariantList>


class Utils
{

public:
    Utils();
    ~Utils();
    static QString int2Str(int i);
    static QString long2Str(long l);
    static QString float2Str(float f);
    static QString double2Str(double b);
    static QString char2Str(char* c);
    static QString bool2Str(bool b);
    static QString dataContain(QVariantList list,QString split);
    static QString dataContain(QVariantList list);
    static QString today();

    static QString Q2Q(QString str);
    static QString C2Q(char * str);
    static char  * Q2C(QString str);
    static char  * Q2C(QVariant str);


};

#endif // UTILS_H
