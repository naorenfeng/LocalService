#ifndef ___BASE64_H___
#define ___BASE64_H___


#include <string>
#include <QString>

using namespace std;

class Base64
{
public:
    Base64();
    ~Base64();

    /****
     * @brief KqEncrypt             加密
     * @param strInfo               传入需要加密的字符串
     * @return                      返回加密后的字符串
     */
    static QString Encrypt(QString strInfo);
    static void Encrypt_s(QString &strInfo);

    /****
     * @brief KqDecrypt             解密
     * @param strInfo               传入需要解密的字符串
     * @return                      返回解密后的字符串
     */
    static QString Decrypt(QString strInfo);
    static void Decrypt_s(QString &strInfo);


};

#endif // ___BASE64_H___
