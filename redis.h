#ifndef REDIS_H
#define REDIS_H

#include <hiredis.h>
#include <QObject>
#include <QString>

class Redis
{
public:
    Redis();
    Redis(QString ip,int port);
    ~Redis();

    void setAddress(QString ip,int port);

    void test();

    void set(QString key,QString value);

    QString get(QString key);

    // 连接 redis
    void connectRedis();

    // 执行 redis命令
    void execRedis();

    // 释放资源
    void freeRedis();

    // 释放 reply 指针
    void freeReply();


    //是否连接成功
    bool connected;

    // redis 句柄
    redisContext *m_context;

    // redis 执行返回数据
    redisReply *m_reply;

    // 执行命令的 cmd 字符串
    QString m_cmd;

    // 连接 ip，端口
    QString m_ip;
    int m_port;
    //默认超时时间
    QString expireTime = "60";
};

#endif // REDIS_H
